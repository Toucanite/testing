﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;

namespace TestingProj
{
	class MainClass
	{
		static string str =string.Empty;
		public static void Main (string[] args)
		{
			Stopwatch sw = new Stopwatch ();
			sw.Start ();
			List<int[]> testList = new List<int[]>();
			Random r = new Random ();
			long t = 1000000;

			for (int i = 0; i < 20; ++i) {
				testList.Add(new int[t]);
				int[] tempo = testList.ToArray () [i];
				for (long y = 0; y < t; y++) {
					tempo[y] = r.Next(100);
				}
			}

			/*
			for (int i = 0; i < 20; i++) {
				testList.Add(new int[t]);
			}
			Parallel.ForEach (testList, delegate(int[] tempo) {
				//int[] tempo = testList.ToArray () [i];
				for (long y = 0; y < t; y++) {
					tempo[y] = r.Next(100);
				}
				
			});
			*/
			List<int> yaya = new List<int> ();
			/*
			Parallel.ForEach (testList, delegate(int[] ob) {
				
				Parallel.ForEach(ob, delegate(int meh) {
					//str += meh.ToString()+" ";
					lock (yaya) {
						yaya.Add(meh);	
					}
				});
			/*
				foreach (var meh in ob) {
					try {
						Monitor.Enter(yaya);
						yaya.Add(meh);

					} finally {
						Monitor.Exit(yaya);
					}
				}
			});
			*/
			foreach (int[] ob in testList) {
				foreach (int meh in ob) {
					yaya.Add(meh);
				}
			}

			Console.WriteLine (yaya.Count.ToString ());
			Console.WriteLine (sw.Elapsed.TotalSeconds.ToString());
			//Console.WriteLine (str);
			yaya.Sort ();
			int temp = -1;
			int cumul = 0;
			string str = string.Empty;
			foreach (int item in yaya) {
				if (item != temp) {
					int nbr = yaya.FindAll ((int obj) => obj == item).Count;
					cumul += nbr;
					str += (item.ToString() +"  :  "+ nbr) + Environment.NewLine;
				}
				temp = item;
			}
			sw.Stop ();
			Console.WriteLine (str);
			Console.WriteLine (cumul.ToString());
			Console.WriteLine (sw.Elapsed.TotalSeconds.ToString());
		}
	}
}

